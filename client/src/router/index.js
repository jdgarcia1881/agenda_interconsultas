import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Profile from '@/components/Profile'
import Patient from '@/components/Patient'
import PatientList from '@/components/PatientList'
import Talk from '@/components/Talk'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/patient',
      name: 'Patient',
      component: Patient
    },
    {
      path: '/patientlist',
      name: 'PatientList',
      component: PatientList
    },
    {
      path: '/talk',
      name: 'Talk',
      component: Talk
    }
  ]
})
