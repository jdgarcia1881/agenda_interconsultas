**Iniciar carpeta server:**

adonis serve --dev

**Iniciar carpeta client:**

npm start

**Documentación AdonisJS:**

https://adonisjs.com/docs/4.1/installation

**Lista de comandos de ayuda:**

adonis --help