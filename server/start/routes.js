'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
    //User
    Route.post('login', 'UserController.login')
    Route.post('register', 'UserController.register')
    Route.get('getuser/:id', 'UserController.show')
    //Patient
    Route.get('patients', 'PatientController.index')
    Route.post('patient', 'PatientController.register')
    Route.get('patient/:id', 'PatientController.show')
    Route.put('patient/:id', 'PatientController.update')
    Route.delete('patient/:id', 'PatientController.delete')


    //Talk
    Route.get('talks', 'PatientController.index')
    Route.post('newtalk', 'TalkController.register')
}).prefix('users')

