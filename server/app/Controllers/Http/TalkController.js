'use strict'

const Talk = use('App/Models/Talk')

class TalkController {
    async register({ request, response }) {
        const talkInfo = request.only(['title', 'description'])
        await Talk.create(talkInfo);
        
        return response.send({ message: 'Talk has been created' })
    }

    async show({params, response}) {
        const talk = await Talk.find(params.id);
        return response.json(talk);
    }
}

module.exports = TalkController
