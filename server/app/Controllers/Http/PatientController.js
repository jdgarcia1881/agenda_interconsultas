'use strict'

const Patient = use('App/Models/Patient')

class PatientController {
    async index({ request, response })  {
        const page = request.get('page', 1);
        const patients = await Patient
        .query()
        .with('documentType')
        .with('gender')
        .with('regime')
        .with('zone')
        .with('disability')
        .with('education')
        .with('populationGroup')
        .with('city.province.country')
        .paginate(page)
        response.ok(patients);
    }

    async register({ request, response }) {
        const patientInfo = request.only(['document_type_id', 'document', 'first_name', 'middle_name',
        'surname', 'second_surname', 'birth_date', 'gender_id', 'regime_id', 'zone_id', 'disability_id',
        'educational_level_id', 'population_group_id', 'landline', 'first_cell_phone', 
        'second_cell_phone', 'email', 'city_id', 'address'])
        await Patient.create(patientInfo);
        
        return response.send({ message: 'Patient has been created' })
    }

    async show({params, response}) {
        const patient = await Patient.find(params.id);
        return response.json(patient);
    }

    async update({ params, request, response }) {
        const patientInfo = request.only(['document_type_id', 'document', 'first_name', 'middle_name',
        'surname', 'second_surname', 'birth_date', 'gender_id', 'regime_id', 'zone_id', 'disability_id',
        'educational_level_id', 'population_group_id', 'landline', 'first_cell_phone', 
        'second_cell_phone', 'email', 'city_id', 'address']);
    
        const patient = await Patient.find(params.id);
        if (!patient) {
            return response.status(404).json({ data: 'Datos de paciente no encontrados' });
        }
        patient.document_type_id = patientInfo.document_type_id;
        patient.document = patientInfo.document;
        patient.first_name = patientInfo.first_name;
        patient.middle_name = patientInfo.middle_name;
        patient.surname = patientInfo.surname;
        patient.second_surname = patientInfo.second_surname;
        patient.birth_date = patientInfo.birth_date;
        patient.gender_id = patientInfo.gender_id;
        patient.regime_id = patientInfo.regime_id;
        patient.zone_id = patientInfo.zone_id;
        patient.disability_id = patientInfo.disability_id;
        patient.educational_level_id = patientInfo.educational_level_id;
        patient.population_group_id = patientInfo.population_group_id;
        patient.landline = patientInfo.landline;
        patient.first_cell_phone = patientInfo.first_cell_phone;
        patient.second_cell_phone = patientInfo.second_cell_phone;
        patient.email = patientInfo.email;
        patient.city_id = patientInfo.city_id;
        patient.address = patientInfo.address;

        await patient.save();
        return response.status(200).json(patient);
    }
}

module.exports = PatientController
