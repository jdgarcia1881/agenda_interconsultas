'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PopulationGroup extends Model {
    patients() {
        return this.hasMany('App/Models/Patient');
    }
}

module.exports = PopulationGroup
