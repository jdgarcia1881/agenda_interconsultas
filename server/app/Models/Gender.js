'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Gender extends Model {
    patients() {
        return this.hasMany('App/Models/Patient');
    }
}

module.exports = Gender
