'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Patient extends Model {
    documentType() {
        return this.belongsTo('App/Models/DocumentType');
    }
    gender() {
        return this.belongsTo('App/Models/Gender');
    }
    regime() {
        return this.belongsTo('App/Models/Regime');
    }
    zone() {
        return this.belongsTo('App/Models/Zone');
    }
    disability() {
        return this.belongsTo('App/Models/Disability');
    }
    education() {
        return this.belongsTo('App/Models/Education');
    }
    populationGroup() {
        return this.belongsTo('App/Models/PopulationGroup');
    }
    city() {
        return this.belongsTo('App/Models/City');
    }
}

module.exports = Patient
