'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Country extends Model {
    province() {
        return this.hasMany('App/Models/Province');
    }
}

module.exports = Country
